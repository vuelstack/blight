import Vue from 'vue'
import Router from 'vue-router'
import UserLoginManager from '@/Application/Users/Apps/UserLoginManager'
import DashboardMainManager from '@/Application/Dashboards/Apps/DashboardMainManager'
import RegisterMainManager from '@/Application/Registers/Apps/RegisterMainManager'
import PolyMainManager from '@/Application/Polys/Apps/PolyMainManager'
import MedicineMainManager from '@/Application/Medicines/Apps/MedicineMainManager'
import DoctorMainManager from '@/Application/Doctors/Apps/DoctorMainManager'
import PatientMainManager from '@/Application/Patients/Apps/PatientMainManager'

Vue.use(Router)

let router = new Router({
  routes: [
    {
      path: '/',
      name: 'UserLoginManager',
      component: UserLoginManager,
      meta: {
        requiresAuth: false,
        loggedCheck: true
      }
    },
    {
      path: '/dashboards',
      name: 'DashboardMainManager',
      component: DashboardMainManager,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/registers',
      name: 'RegisterMainManager',
      component: RegisterMainManager,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/poly',
      name: 'PolyMainManager',
      component: PolyMainManager,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/medicines',
      name: 'MedicineMainManager',
      component: MedicineMainManager,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/doctors',
      name: 'DoctorMainManager',
      component: DoctorMainManager,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/patients',
      name: 'PatientMainManager',
      component: PatientMainManager,
      meta: {
        requiresAuth: true
      }
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.loggedCheck)) {
    if (localStorage.getItem('token') !== null) {
      next({
        name: 'DashboardMainManager'
      })
    } else {
      next()
    }
  }

  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (localStorage.getItem('token') === null) {
      next({
        path: '/'
      })
    } else {
      next()
    }
  } else {
    next()
  }
})

export default router
