const ROOT_API = `${process.env.ROOT_API}`
const APP = `recipes`
const ENDPOINT = `${ROOT_API}/${APP}/`

export default {
  data () {
    return {
      recipes: [],
      recipe: undefined,
      paginationRecipes: {
        next: undefined,
        previous: undefined,
        count: 0
      },
      queryRecipes: {
        page: 1
      }
    }
  },
  methods: {
    listRecipes () {
      return this.axios.get(ENDPOINT, {params: this.queryRecipes})
        .then((response) => {
          this.recipes = response.data.results
          this.paginationRecipes.next = response.data.next
          this.paginationRecipes.previous = response.data.previous
          this.paginationRecipes.count = response.data.count
        })
    },
    retrieveRecipes (id) {
      return this.axios.get(`${ENDPOINT}${id}/`)
        .then((response) => {
          this.recipe = response.data
        })
    },
    updateRecipes (id, payload) {
      return this.axios.patch(`${ENDPOINT}${id}/`, payload)
        .then((response) => {
          this.recipe = response.data
        })
    },
    createRecipes (payload) {
      return this.axios.post(ENDPOINT, payload)
        .then((response) => {
          this.recipe = response.data
        })
    },
    destroyRecipes (id) {
      return this.axios.delete(`${ENDPOINT}${id}/`)
        .then((response) => {
          this.recipe = undefined
        })
    },
    exportRecipes (prefix) {
      const filename = `${prefix}_${new Date()}_.csv`
      return this.axios({
        url: `${ENDPOINT}export/`,
        method: 'GET',
        responseType: 'blob',
        params: this.queryRecipes
      })
        .then((response) => {
          const url = window.URL.createObjectURL(new Blob([response.data]))
          const link = document.createElement('a')
          link.href = url
          link.setAttribute('download', filename)
          document.body.appendChild(link)
          link.click()
        })
    }
  }
}
