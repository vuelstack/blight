const ROOT_API = `${process.env.ROOT_API}`
const APP = `registers`
const ENDPOINT = `${ROOT_API}/${APP}/`

export default {
  data () {
    return {
      registers: [],
      register: undefined,
      paginationRegister: {
        next: undefined,
        previous: undefined,
        count: 0
      },
      queryRegister: {
        page: 1
      }
    }
  },
  methods: {
    listRegister () {
      return this.axios.get(ENDPOINT, {params: this.queryRegister})
        .then((response) => {
          this.registers = response.data.results
          this.paginationRegister.next = response.data.next
          this.paginationRegister.previous = response.data.previous
          this.paginationRegister.count = response.data.count
        })
    },
    retrieveRegister (id) {
      return this.axios.get(`${ENDPOINT}${id}/`)
        .then((response) => {
          this.register = response.data
        })
    },
    updateRegister (id, payload) {
      return this.axios.patch(`${ENDPOINT}${id}/`, payload)
        .then((response) => {
          this.register = response.data
        })
    },
    createRegister (payload) {
      return this.axios.post(ENDPOINT, payload)
        .then((response) => {
          this.register = response.data
        })
    },
    destroyRegister (id) {
      return this.axios.delete(`${ENDPOINT}${id}/`)
        .then((response) => {
          this.register = undefined
        })
    },
    exportRegister (prefix) {
      const filename = `${prefix}_${new Date()}_.csv`
      return this.axios({
        url: `${ENDPOINT}export/`,
        method: 'GET',
        responseType: 'blob',
        params: this.queryRegister
      })
        .then((response) => {
          const url = window.URL.createObjectURL(new Blob([response.data]))
          const link = document.createElement('a')
          link.href = url
          link.setAttribute('download', filename)
          document.body.appendChild(link)
          link.click()
        })
    }
  }
}
